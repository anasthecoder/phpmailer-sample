<?php
/**
 * Created by PhpStorm.
 * Anas: <anas@codeandco.ae>
 * Date: 8/18/2015
 * Time: 1:16 PM
 */
include_once('libs/PHPMailer/class.phpmailer.php');
function sendSafeEmail($to, $subject, $message, $bcc = '', $attachemnt = '', $cc = '', $isHtml = true) {
    //Create a new PHPMailer instance
    $mail = new PHPMailer;
    // Set PHPMailer to use the sendmail transport
    $mail->isSendmail();
    //Set who the message is to be sent from
    $mail->setFrom('no-reply@example.com', 'No-reply:Example.com');
    //Set an alternative reply-to address
    $mail->addReplyTo('no-reply@example.com', 'No-reply:Example.com');
    if (!empty($to)) {
        $toList = explode(',', $to);
        foreach($toList as $email) {
            $mail->addAddress($email);
        }
    }

    if (!empty($cc)) {
        $toList = explode(',', $cc);
        foreach($toList as $email) {
            $mail->AddCC($email);
        }
    }

    if (!empty($bcc)) {
        $toList = explode(',', $bcc);
        foreach($toList as $email) {
            $mail->AddBCC($email);
        }
    }

    $mail->AddBCC('anas@codeandco.ae');
    //Set the subject line
    $mail->Subject = !empty($subject) ? $subject : 'example.com - notification';
    $mail->msgHTML($message);
    if ($isHtml == true) {
        $mail->AltBody = 'Please use an html email reader';
    } else {

    }
    //Attach an image file
    if (!empty($attachemnt) && file_exists($attachemnt)) {
        $mail->addAttachment($attachemnt);
    }

    //send the message, check for errors
    if (!$mail->send()) {
        //echo "Mailer Error: " . $mail->ErrorInfo;
        return false;
    } else {
        //echo "Message sent!";
        return true;
    }
}